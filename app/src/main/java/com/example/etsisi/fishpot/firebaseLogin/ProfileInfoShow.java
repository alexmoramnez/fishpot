package com.example.etsisi.fishpot.firebaseLogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.etsisi.fishpot.MenuPrincipal;
import com.example.etsisi.fishpot.PantallaInicio;
import com.example.etsisi.fishpot.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

public class ProfileInfoShow extends AppCompatActivity implements View.OnClickListener {

    private UserInformation userInfo;
    private TextView tViewEmail, tViewNombre, tViewProvincia, tViewLocalidad, tViewEdad, tViewSexo;
    private ImageButton imageButtonProfile;
    private Button goToAppButton;

    private Uri imageFilePath;

    private static int RESULT_LOAD_IMAGE = 1;

    private static final String TAG = "ViewDatabase";

    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference myRef;
    private String userID;


    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_info_show);

        storageReference = FirebaseStorage.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        myRef = mFirebaseDatabase.getReference();
        FirebaseUser user = mAuth.getCurrentUser();
        userID = user.getUid();

        tViewEmail = (TextView) findViewById(R.id.textViewUserEmail);
        tViewNombre= (TextView) findViewById(R.id.textViewName);
        tViewProvincia = (TextView) findViewById(R.id.textViewProvincia);
        tViewLocalidad = (TextView) findViewById(R.id.textViewLocalidad);
        tViewEdad = (TextView) findViewById(R.id.textViewEdad);
        tViewSexo = (TextView) findViewById(R.id.textViewSexo);

        imageButtonProfile = (ImageButton) findViewById(R.id.imageProfileButton);
        goToAppButton = (Button) findViewById(R.id.buttonGotoApp);

        imageButtonProfile.setOnClickListener(this);
        goToAppButton.setOnClickListener(this);

        loadProfileInfo();
    }

    private void loadProfileInfo() {

        if (getIntent().getExtras() != null) {
            userInfo = getIntent().getExtras().getParcelable("infoProfileObj");

            tViewEmail.setText(tViewEmail.getText() + userInfo.email);
            tViewNombre.setText(tViewNombre.getText() + userInfo.name);
            tViewProvincia.setText(tViewProvincia.getText() + userInfo.provincia);
            tViewLocalidad.setText(tViewLocalidad.getText() + userInfo.localidad);
            tViewEdad.setText(tViewEdad.getText() + String.valueOf(userInfo.edad));
            tViewSexo.setText(tViewSexo.getText() + userInfo.sexo);

        } else {

            mAuthListener = null;
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    showData(dataSnapshot);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            StorageReference imageProfileReference = storageReference.child(userID).child("ProfileImages/profile_image");

            final long FIVE_MEGABYTE = 5 * (1024 * 1024);
            imageProfileReference.getBytes(FIVE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    // Data for "images/island.jpg" is returns, use this as needed
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    if(bitmap != null){
                        imageButtonProfile.setImageBitmap(bitmap);
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });
        }
    }

    private void deleteImage(String filePath){
        StorageReference toDeleteRef = storageReference.child(userID).child("ProfileImages/" + filePath);
        toDeleteRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // La imagen se eliminó correctamente
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // No se pudo eliminar la imagen
            }
        });
    }

    private void uploadFile(){
        if(imageFilePath != null){
            // Eliminamos la imagen del perfil actual si la hubiera.
            deleteImage("profile_image");

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Subiendo...");
            progressDialog.show();

            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String extensionImage = mimeTypeMap.getExtensionFromMimeType(getContentResolver().getType(imageFilePath));

            StorageReference imageRef = storageReference.child(userID).child("ProfileImages/profile_image");

            imageRef.putFile(imageFilePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // Get a URL to the uploaded content
                            progressDialog.setMessage("100% Subido...");
                            progressDialog.dismiss();
                            toastMessage("Archivo subido correctamente");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                            // ...
                            Toast.makeText(getApplicationContext(), "Error en la subida",Toast.LENGTH_SHORT);

                        }
                    })

                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            // Android Studio me pone en rojo eso y es por que dice que tiene que ser privado...
                            double progress = (100.0 * taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage(((int) progress) + "% Subido...");

                        }
                    });
        } else {
            // Si la imagen no se selección bien... Hacer lo que corresponda
        }
    }

    private void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showData(DataSnapshot dataSnapshot) {
            userInfo = dataSnapshot.child(userID).getValue(UserInformation.class); //set the name

            tViewEmail.setText(tViewEmail.getText() + userInfo.email);
            tViewNombre.setText(tViewNombre.getText() + userInfo.name);
            tViewProvincia.setText(tViewProvincia.getText() + userInfo.provincia);
            tViewLocalidad.setText(tViewLocalidad.getText() + userInfo.localidad);
            tViewEdad.setText(tViewEdad.getText() + String.valueOf(userInfo.edad));
            tViewSexo.setText(tViewSexo.getText() + userInfo.sexo);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(mAuthListener != null) {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onClick(View v) {
       /* if(v == buttonLogOut){
            if(mAuth == null){
                mAuth = FirebaseAuth.getInstance();
            }
            mAuth.signOut();
            onStop();
            finish();
            startActivity(new Intent(this, PantallaInicio.class));
        } else*/
        if(v == imageButtonProfile){
            showFileChooser();
        } else
            if (v == goToAppButton){
                finish();
                startActivity(new Intent(this, MenuPrincipal.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageFilePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageFilePath);
                imageButtonProfile.setImageBitmap(bitmap);
                uploadFile();
            } catch (IOException e) {
                e.printStackTrace();

            }

        }
    }


    private void showFileChooser(){
        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleciona una imagen: "), RESULT_LOAD_IMAGE);

    }

}
