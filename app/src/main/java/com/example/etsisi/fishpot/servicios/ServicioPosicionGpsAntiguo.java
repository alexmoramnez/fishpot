package com.example.etsisi.fishpot.servicios;

import android.app.AlertDialog;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;


import com.example.etsisi.fishpot.util.GoogleMapsUtil;
import com.example.etsisi.fishpot.MainActivity;
import com.example.etsisi.fishpot.R;
import com.example.etsisi.fishpot.modelos.SitioPesca;

import android.location.LocationListener;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.kml.KmlLayer;
import com.google.maps.android.kml.KmlPlacemark;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * Servicio que detecta la posición del telefono y lanza la aplicación si la posición esta cerca
 * de un sitio de pesca
 */
public class ServicioPosicionGpsAntiguo extends IntentService {

    /**
     * Radio de busqueda
     */
    public static int METROS_BUSQUEDA = 1000;

    /**
     * Numero de metros  a comprobar si hay nuevos sitios de pesca
     */
    private static final float METROSCOMPROBAR = 500;
    /**
     * Parametro con el valor del tiempo de espera que se le pasa en el intent
     */
    public static String parametroTiempoEspera = "tiempoEspera";

    /**
     * Equivalencia en segundos de un minuto
     */
    private static final int MINUTOS = 60;

    /**
     * Equivalencia de milisegundos de un segundo
     */
    private static final int MILISEGUNDOS = 1000;
    /**
     * Tiempo de espera hasta que se vuelve otra vez a comprobar la posición, en minutos
     */
    private long tiempoEspera;

    private GoogleMap googleMap;


    private class LocalizadorSitiosPesca implements LocationListener {


        private  List<SitioPesca> listaSitioPescas;
        /**
         * Mapa ordenado con los sitios de pesca por cercania al dipositivo
         */
        private TreeMap<Double,SitioPesca> mapaOrdenadoSitiosPesca;
        public LocalizadorSitiosPesca(List<SitioPesca> listaSitiosPesca) {
            this.listaSitioPescas = listaSitiosPesca;
            mapaOrdenadoSitiosPesca = new TreeMap<Double,SitioPesca>();

        }

        /**
         * Called when the location has changed.
         * <p>
         * <p> There are no restrictions on the use of the supplied Location object.
         *
         * @param location The new location, as a Location object.
         */
        @Override
        public void onLocationChanged(Location location) {

            //Obtenemos la posicion actual
            LatLng posicionACtual = new LatLng(location.getLatitude(),location.getLongitude());
            //Recorremos la lista de sitios  calculando la distancia a los sitios que estan dentro del radio de busqueda
            for( SitioPesca sitio : listaSitioPescas)
            {
                double distancia =this.calcularDistanciaPuntos(posicionACtual,sitio.getLatLng());
                if( distancia<=METROS_BUSQUEDA)
                {//el sitio esta dentro del radio de busqueda
                    mapaOrdenadoSitiosPesca.put(distancia,sitio);
                }
            }

            //Lanzamos una notificacion al movil diciendo si quiere iniciar la aplicación con el primer  sitio del mapa(el sitio mas proximo)
            if(!mapaOrdenadoSitiosPesca.isEmpty()) {
                this.lanzarAplicacion(mapaOrdenadoSitiosPesca.firstEntry().getKey(), mapaOrdenadoSitiosPesca.firstEntry().getValue());
            }


        }

        /**
         * Se lanza la aplicación con el sitio mas próximo
         * @param distancia distancia del usuario
         * @param sitio
         */
        private void lanzarAplicacion(Double distancia, SitioPesca sitio) {

            //creamos la notificación
            //creamos la accion
            Intent intentLanzaApp = new Intent(ServicioPosicionGpsAntiguo.this.getApplicationContext(), MainActivity.class);
            intentLanzaApp.putExtra("nombreSitio",sitio.getNombre());
            intentLanzaApp.putExtra("pagina",sitio.getUrlPagina());

            PendingIntent resultadoIntent = PendingIntent.getActivity(ServicioPosicionGpsAntiguo.this.getApplicationContext(),0,intentLanzaApp,PendingIntent.FLAG_UPDATE_CURRENT);
            //creamos la notificacion
            Notification notificacion =new  NotificationCompat.Builder(ServicioPosicionGpsAntiguo.this.getApplicationContext()).setContentTitle(ServicioPosicionGpsAntiguo.this.getResources().getString(R.string.app_name)).setContentText("El sitio "+sitio.getNombre()+" esta a "+distancia+". Click para abrir la aplicacion").setWhen(System.currentTimeMillis()).setContentIntent(resultadoIntent).build();
            //Lanzamos la notificion
            NotificationManager notificationManager = (NotificationManager) ServicioPosicionGpsAntiguo.this.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(001,notificacion);


        }

        private double calcularDistanciaPuntos(LatLng puntoInicio, LatLng puntoFinal) {
            int Radius = 6371;// radius of earth in Km
            double lat1 = puntoInicio.latitude;
            double lat2 = puntoFinal.latitude;
            double lon1 = puntoInicio.longitude;
            double lon2 = puntoFinal.longitude;
            double dLat = Math.toRadians(lat2 - lat1);
            double dLon = Math.toRadians(lon2 - lon1);
            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                    + Math.cos(Math.toRadians(lat1))
                    * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                    * Math.sin(dLon / 2);
            double c = 2 * Math.asin(Math.sqrt(a));
            double valueResult = Radius * c;
            double km = valueResult / 1;
            DecimalFormat newFormat = new DecimalFormat("####");
            int kmInDec = Integer.valueOf(newFormat.format(km));
            double meter = valueResult % 1000;
            int meterInDec = Integer.valueOf(newFormat.format(meter));
            System.out.println("Radius Value:" + valueResult + "   KM  " + kmInDec
                    + " Meter   " + meterInDec);

            return Radius * c;

        }

        /**
         * Called when the provider status changes. This method is called when
         * a provider is unable to fetch a location or if the provider has recently
         * become available after a period of unavailability.
         *
         * @param provider the name of the location provider associated with this
         *                 update.
         * @param status   {@link LocationProvider#OUT_OF_SERVICE} if the
         *                 provider is out of service, and this is not expected to change in the
         *                 near future; {@link LocationProvider#TEMPORARILY_UNAVAILABLE} if
         *                 the provider is temporarily unavailable but is expected to be available
         *                 shortly; and {@link LocationProvider#AVAILABLE} if the
         *                 provider is currently available.
         * @param extras   an optional Bundle which will contain provider specific
         *                 status variables.
         *                 <p>
         *                 <p> A number of common key/value pairs for the extras Bundle are listed
         *                 below. Providers that use any of the keys on this list must
         *                 provide the corresponding value as described below.
         *                 <p>
         *                 <ul>
         *                 <li> satellites - the number of satellites used to derive the fix
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        /**
         * Called when the provider is enabled by the user.
         *
         * @param provider the name of the location provider associated with this
         *                 update.
         */
        @Override
        public void onProviderEnabled(String provider) {

        }

        /**
         * Called when the provider is disabled by the user. If requestLocationUpdates
         * is called on an already disabled provider, this method is called
         * immediately.
         *
         * @param provider the name of the location provider associated with this
         *                 update.
         */
        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    /**
     * Lista de los sitios de pesca
     */
    private List<SitioPesca> listaSitiosPesca;

    public ServicioPosicionGpsAntiguo() {
        super("ServicioPosicionGpsAntiguo");
    }


    @Override
    public void onCreate() {
        android.os.Debug.waitForDebugger();
        this.obtenerSitiosPesca();
        super.onCreate();

        //Por defecyo deginimos que el tiempo de espera son 10 minutos
        this.tiempoEspera = 10 * MINUTOS;

        //inicializamos el cliente de googleMaps
        /*this.clienteGoogleAPI = new  GoogleApiClient
            .Builder(this)
            .addApi(Places.GEO_DATA_API)
            .addApi(Places.PLACE_DETECTION_API)
            .build();
        */

        //Cargamos la capa contenida en el kmz

    }

    /**
     * Se obtiene los sitios de pesca del archivo kmz si la lista esta vacia
     */
    private void obtenerSitiosPesca() {

        if (listaSitiosPesca != null && !listaSitiosPesca.isEmpty()) {//La lista ya tiene elementos
            Log.d(ServicioPosicionGpsAntiguo.class.getName(), "La lista ya estaba creada de antes");
        } else {
            if (listaSitiosPesca == null) {
                listaSitiosPesca = new ArrayList<SitioPesca>();
            }
            //rellenamos la lista con los elementos de la capa kmz
            this.rellenarListar();
        }

    }

    /**
     * Se obtiene el objeto kmlLayer y se rellena la lista de sitios con los marcadores que estan
     * en ese archivo almacenado
     */
    private void rellenarListar() {
        final MapFragment mapa = new MapFragment();
        final KmlLayer[] capaSitios = new KmlLayer[1];
        mapa.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap googleMap) {

                ServicioPosicionGpsAntiguo.this.googleMap = googleMap;
                //cargamos la capa
                try {
                    capaSitios[0] = new KmlLayer(googleMap, R.raw.kmlfile, getApplicationContext());

                } catch (XmlPullParserException e) {
                    Log.d(ServicioPosicionGpsAntiguo.class.getName(), "Se ha producido un error al leer el archivo: " + e);
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.d(ServicioPosicionGpsAntiguo.class.getName(), "Se ha podido acceder al archivo kmz: " + e);
                    e.printStackTrace();
                }
            }
        });
        KmlLayer capaSitiosPesca = capaSitios[0];

        for (KmlPlacemark marca : capaSitiosPesca.getPlacemarks()) {
            SitioPesca sitio = GoogleMapsUtil.crearSitioPesca(marca);
            if(sitio != null)
            {
                listaSitiosPesca.add(sitio);
            }
        }

    }

    @Override
    protected void onHandleIntent(Intent intent) {


        if (intent != null) {
            //Cambiamos el tiempo de espera si se ha especificado en el intent
            this.tiempoEspera = intent.getLongExtra(ServicioPosicionGpsAntiguo.parametroTiempoEspera, tiempoEspera);
            // TODO: Implementar esta comprobacion si hay tiempo
            final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            //comprobamos si el gps está activado
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                //Lanzamos un aviso de que el gps no está activado,
                this.lanzarAvisoGpsDesactivado();
                while (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    //TODO:bloquear el hilo con un wait  y esperamos  a que se active el gps para seguir funcionando el servicio
                    new Thread() {
                        public void run() {
                            while (true) {
                                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                                    //se ha activado el gps, despertamos al hilo
                                    ServicioPosicionGpsAntiguo.this.notify();
                                }
                            }
                        }
                    }.start();

                    try {
                        this.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Log.d(ServicioPosicionGpsAntiguo.class.getName(), "Se ha interrupido la espera a que se active el gps: " + e);
                    }

                }
            }
                //TODO: Hacer la magia
                LocationListener localizadorSitiosPesca = new LocalizadorSitiosPesca(this.listaSitiosPesca);
                //Lanzamos el listener para detectar sitios de pesca

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, this.tiempoEspera * MINUTOS * MILISEGUNDOS, METROSCOMPROBAR, localizadorSitiosPesca);

        }
        else
        {
            Log.d(ServicioPosicionGpsAntiguo.class.getName(),"AL servicio se le ha pasado un intent null");
        }
    }

    private void lanzarAvisoGpsDesactivado() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.activarGPS)
                .setCancelable(false)
                .setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {

                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }


}

