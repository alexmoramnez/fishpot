package com.example.etsisi.fishpot.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/**
 * Documentacion:
 * 
 * https://jsoup.org/
 * https://jsoup.org/cookbook/extracting-data/selector-syntax
 * 
 * @author Riki Gomez
 *
 */
public class ParserJSoup {
    /**
     * Test de la clase
     * @param args
     */
    /**
    public static void main(String[] args) {
        try {
            String doc = "http://haypesca.blogspot.com.es/2015/10/embalse-de-beznar-granada.html";
            System.out.println(obtenerListaEspeciesPescables(doc));
            System.out.println(obtenerListaTecnicas(doc));
            System.out.println(obtenerInformacionUtil(doc));
            System.out.println(obtenerListaImagenes(doc));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }**/



    private static Document getDocument(String html) throws IOException {
        return Jsoup.connect(html).get();
    }
    public static List < String > obtenerListaEspeciesPescables(String html) throws IOException {

        Document doc = getDocument(html);
        List < String > listaEspeciesPescables = new ArrayList < String > ();
        Elements elementosSpan = doc.select("span");
        int indiceInicioEspecies = 0;
        for (Element elemento: elementosSpan) {
            indiceInicioEspecies++;
            String textoEtiqueta = elemento.text();
            if (textoEtiqueta.contains("Especies Pescables:")) {
                break;
            }
        }
        boolean finEspecies = false;
        indiceInicioEspecies++;
        while (!finEspecies) {
            String textoContenido = elementosSpan.get(indiceInicioEspecies).text();
            if (textoContenido.contains("Técnicas y cebos principales:")) {
                finEspecies = true;
            } else {

                listaEspeciesPescables.add(textoContenido.replaceAll("•", "").replaceAll("\u00a0", ""));
                indiceInicioEspecies++;
            }
        }
        return listaEspeciesPescables;
    }

    public static List < String > obtenerListaTecnicas(String html) throws IOException {
        Document doc = getDocument(html);
        List < String > listaTecnicas = new ArrayList < String > ();
        Elements elementosSpan = doc.select("span");
        int indiceInicioTecnicas = 0;
        for (Element elemento: elementosSpan) {
            indiceInicioTecnicas++;
            String textoEtiqueta = elemento.text();
            if (textoEtiqueta.contains("Técnicas y cebos principales:")) {
                break;
            }
        }
        boolean finEspecies = false;
        indiceInicioTecnicas++;
        while (!finEspecies) {
            String textoContenido = elementosSpan.get(indiceInicioTecnicas).text();
            if (textoContenido.contains("Información útil:")) {
                finEspecies = true;
            } else {

                listaTecnicas.add(textoContenido.replaceAll("•", "").replaceAll("\u00a0", ""));
                indiceInicioTecnicas++;
            }
        }
        return listaTecnicas;
    }

    public static String obtenerInformacionUtil(String html) throws IOException {
        Document doc = getDocument(html);
        Elements elementosSpan = doc.select("span");
        int indiceInicioInfoUtil = 0;
        for (Element elemento: elementosSpan) {
            indiceInicioInfoUtil++;
            String textoEtiqueta = elemento.text();
            if (textoEtiqueta.contains("Información útil:")) {
                break;
            }
        }

        indiceInicioInfoUtil++;

        //	    Element elementoInfo = elementosSpan.get(indiceInicioInfoUtil);
        //	    StringBuilder stringBuilder = new StringBuilder();
        //	    for(Element hijo : elementoInfo.getAllElements())
        //	    {
        //		stringBuilder.append(hijo.text());
        //	    }

        String textoContenido = "";
        try {
            textoContenido = elementosSpan.get(indiceInicioInfoUtil).text();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }


        return textoContenido;
    }

    public static List < String > obtenerListaImagenes(String html) throws IOException {
        Document doc = getDocument(html);
        List < String > listaImagenes = new ArrayList < String > ();
        Elements elementosIMG = doc.select("#post-wrapper img[src$=JPG]");
        for (Element elemento: elementosIMG) {

            listaImagenes.add(elemento.attr("src"));
        }
        return listaImagenes;
    }




}