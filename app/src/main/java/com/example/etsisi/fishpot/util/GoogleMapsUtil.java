package com.example.etsisi.fishpot.util;

import android.util.Log;
import android.util.Patterns;

import com.example.etsisi.fishpot.modelos.SitioPesca;
import com.example.etsisi.fishpot.servicios.ServicioPosicionGpsAntiguo;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.maps.android.kml.KmlPlacemark;
import com.google.maps.android.kml.KmlPoint;

import java.util.regex.Matcher;

/**
 * Created by Riki Gomez on 25/04/2017.
 */

public class GoogleMapsUtil {

    public static MarkerOptions getMarkOfKmlPlaceMark(KmlPlacemark marcaKml)
    {
        KmlPoint puntoMapa = (KmlPoint) marcaKml.getGeometry();
        String descricion = marcaKml.getProperty(SitioPesca.propiedadDescripccion).replace("<br>","").split("Ficha en Hay Pesca!")[0];
        MarkerOptions marcaMap = new MarkerOptions().position(puntoMapa.getGeometryObject()).title(marcaKml.getProperty(SitioPesca.propiedadNombre)).snippet(descricion);
        return marcaMap;
    }

    public static SitioPesca crearSitioPesca(String nombre,String descripcion, String coordenadas)
    {
        String url = obtenerUrlSitio(descripcion);
        String[] arrayCoordenas = coordenadas.split(",");

        LatLng latLng = new LatLng(Double.parseDouble(arrayCoordenas[1]),Double.parseDouble(arrayCoordenas[0]));
        KmlPoint puntoCoord = new KmlPoint(latLng);

        SitioPesca sitio = new SitioPesca(nombre,url,puntoCoord,descripcion);

        return sitio;
    }

    private static String obtenerUrlSitio(String descripcion)
    {
        //de la descripcion obtenemos la pagina html
        Matcher matcher = Patterns.WEB_URL.matcher(descripcion);
        StringBuilder stringBuilder = new StringBuilder();
        int i=0;
        while (matcher.find()) {//se ha encontrado la url
            stringBuilder.append(matcher.group(i));
        }
        String url = stringBuilder.toString();
        if(url.charAt(url.length() - 1) == ')')
        {//eliminamos el ultimo parentesis sobrante
            url = url.substring(0,url.length()-1);
        }
        return url;
    }

    public static SitioPesca crearSitioPesca(KmlPlacemark marca) {
        String nombre = marca.getProperty(SitioPesca.propiedadNombre);
        String descripcion = marca.getProperty(SitioPesca.propiedadDescripccion);

        KmlPoint puntoMapa = null;
        String url =  obtenerUrlSitio(descripcion);


        if (marca.getGeometry() instanceof KmlPoint) {
            puntoMapa = (KmlPoint) marca.getGeometry();
        } else {
            Log.d(ServicioPosicionGpsAntiguo.class.getName(),"El objeto geometry de la marca no corresponde a un objeto de tipo Point " + marca);
            //TODO: Conseguir las posiciones de alguna manera para crear el punto
        }
        SitioPesca sitio  = null;
        //creamos el sitio y lo añadimos a la lista si tiene todos sus atributos corrrectos
        if (nombre != null && url != null && puntoMapa != null) {
            sitio = new SitioPesca(nombre, url, puntoMapa,descripcion);
            Log.d(ServicioPosicionGpsAntiguo.class.getName(),"Se ha añadido a la lista  la marca: " + marca);

        } else {
            Log.d(ServicioPosicionGpsAntiguo.class.getName(),"La marca no se ha añadido porque no se ha podido obtener algun atributo. Atributos: nombre=" + nombre + ", url=" + url + ", point=" + puntoMapa + ". Marca:" + marca);
        }
        return sitio;

    }
}
