package com.example.etsisi.fishpot.firebaseLogin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.etsisi.fishpot.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Button buttonRegister;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextRePassword;
    private TextView textViewSignIn;
    private CheckBox condicionesCheckBox;
    private TextView condicionesLink;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;

    private DatabaseReference databaseReference;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        progressDialog = new ProgressDialog(this);

        firebaseAuth = FirebaseAuth.getInstance();

        // Si el usuario ya está logeado, mostramos profile activity.
        if(firebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), ProfileInfoEntry.class ));
        }

        databaseReference = FirebaseDatabase.getInstance().getReference();

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextRePassword = (EditText) findViewById(R.id.editTextRePass);

        condicionesCheckBox = (CheckBox) findViewById(R.id.condicionesCheckBox);
        condicionesLink = (TextView) findViewById(R.id.condicionesLink);

        buttonRegister = (Button) findViewById(R.id.buttonRegister);

        textViewSignIn = (TextView) findViewById(R.id.textViewSignIn);

        buttonRegister.setOnClickListener(this);
        textViewSignIn.setOnClickListener(this);
        condicionesCheckBox.setOnClickListener(this);
        condicionesLink.setOnClickListener(this);

        editTextEmail.setOnFocusChangeListener(this);
        editTextPassword.setOnFocusChangeListener(this);
        editTextRePassword.setOnFocusChangeListener(this);
    }


    private void registerUser(){
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String repassword = editTextRePassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            Toast.makeText(this, "Introduce un email válido", Toast.LENGTH_SHORT).show();
            return;
        }else if(TextUtils.isEmpty(password)){
            Toast.makeText(this, "Introduce una contraseña válida", Toast.LENGTH_SHORT).show();
            return;
        }else if(TextUtils.isEmpty(repassword)) {
            Toast.makeText(this, "Vuelve a introducir la contraseña", Toast.LENGTH_SHORT).show();
            return;
        }else if(!TextUtils.equals(password, repassword)){
            Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Registering User... Please Wait");
        progressDialog.show();


        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //user ir succefully registered and logged in
                            //we will start the profile activity here
                            //right now lets display a toast only
                            Toast.makeText(Register.this, "Registered Succesfully", Toast.LENGTH_SHORT).show();
                            // Si el usuario ya está logeado, mostramos profile activity.
                            finish();
                            startActivity(new Intent(getApplicationContext(), ProfileInfoEntry.class ));
                        } else {
                            progressDialog.cancel();
                            if(password.length() < 6){
                                Toast.makeText(Register.this, "Se necesitan 6 caracteres minimo.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(Register.this, "No se ha podido registrar. Pruebe de nuevo", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        if(v == buttonRegister){
            registerUser();
        } else if(v == textViewSignIn){
            startActivity(new Intent(this, Login.class));
        } else if(v == condicionesCheckBox){
            hideKeyboard(v);
        } else if(v == condicionesLink){
            Toast.makeText(Register.this, "Cuando salga al mercado... Se implementará", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus) {
            hideKeyboard(v);
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
