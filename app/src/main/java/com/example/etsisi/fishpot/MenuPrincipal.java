package com.example.etsisi.fishpot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.etsisi.fishpot.firebaseLogin.ProfileInfoShow;
import com.google.firebase.auth.FirebaseAuth;

public class MenuPrincipal extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout layLocalizacion;
    private LinearLayout layInfoCuenta;
    private LinearLayout layCapturas;
    private LinearLayout layCerrarSesión;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        layLocalizacion = (LinearLayout) findViewById(R.id.linLayLocalicacion);
        layLocalizacion.setOnClickListener(this);

        layInfoCuenta = (LinearLayout) findViewById(R.id.linLayInfoCuenta);
        layInfoCuenta.setOnClickListener(this);

        layCapturas = (LinearLayout) findViewById(R.id.linLayCapturas);
        layCapturas.setOnClickListener(this);

        layCerrarSesión = (LinearLayout) findViewById(R.id.linLayCerrarSesion);
        layCerrarSesión.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == layLocalizacion){
            startActivity(new Intent(this, MapaSitiosPesca.class));

        } else
            if(v == layInfoCuenta){
                startActivity(new Intent(this, ProfileInfoShow.class));
        } else
            if(v == layCapturas){
                Toast.makeText(this, "Proximamente en la V.2", Toast.LENGTH_SHORT).show();
        } else
            if(v == layCerrarSesión){
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                mAuth.signOut();
                finish();
                startActivity(new Intent(this, PantallaInicio.class));

        }
    }
}
