package com.example.etsisi.fishpot;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.etsisi.fishpot.modelos.SitioPesca;
import com.example.etsisi.fishpot.servicios.ServicioPosicionGps;
import com.example.etsisi.fishpot.servicios.ServicioPosicionGpsAntiguo;
import com.example.etsisi.fishpot.util.GoogleMapsUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.kml.KmlContainer;
import com.google.maps.android.kml.KmlLayer;
import com.google.maps.android.kml.KmlPlacemark;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MapaSitiosPesca extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private Map<String, SitioPesca> mapaSitiosPesca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_sitios_pesca);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapaSitiosPesca = new HashMap<String, SitioPesca>();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        this.stopService(new Intent(this,ServicioPosicionGps.class));
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Posicion del centro de españa para el mapa
        LatLng españa = new LatLng(40.4637, -3.7492);


       // ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
       // ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(this,new String[]{ android.Manifest.permission.ACCESS_FINE_LOCATION,android.Manifest.permission.ACCESS_COARSE_LOCATION},101);

            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(españa));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(5));
        //Obtenemos la capa del archivo kml y vamos añadiendo al mapa las marcas definidas en el kml como PlaceMark
        try {
            KmlLayer capaSitios =  new KmlLayer(googleMap, R.raw.kmlfile, getApplicationContext());
            //capaSitios.addLayerToMap();
            //Obtenemos el contenedor con los place mark;
            KmlContainer contenedor = capaSitios.getContainers().iterator().next().getContainers().iterator().next();

            for(KmlPlacemark marca : contenedor.getPlacemarks())
            {
                //creamos el sitio de pesca con la informacion de la pagina
                SitioPesca sitio = GoogleMapsUtil.crearSitioPesca(marca);
                //añadimos el sitio al mapa si se ha creado el sitio correctamente
                if(sitio !=null) {
                    mapaSitiosPesca.put(sitio.getNombre(), sitio);
                }

                //Añadimos el sitio como una marca del mapa
                googleMap.addMarker(GoogleMapsUtil.getMarkOfKmlPlaceMark(marca));
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            Log.d(MapaSitiosPesca.class.getName(), "Se ha producido un error al leer el archivo: " + e);
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(ServicioPosicionGpsAntiguo.class.getName(), "Se ha podido acceder al archivo kmz: " + e);
        }

        //Definimos el evento  correspondiente a la marcas con sitio de pesca
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Toast.makeText(MapaSitiosPesca.this, "Click en el texto para ir a la pagina", Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Toast.makeText(MapaSitiosPesca.this, marker.getTitle(), Toast.LENGTH_SHORT).show();
                Intent intentPaginaPrincipal = new Intent(MapaSitiosPesca.this,MainActivity.class);
                if(mapaSitiosPesca.containsKey(marker.getTitle()))
                {//El mapa de sitios contiene la marca seleccionada
                    SitioPesca sitio = mapaSitiosPesca.get(marker.getTitle());
                    intentPaginaPrincipal.putExtra(SitioPesca.propiedadNombre,sitio.getNombre());
                    intentPaginaPrincipal.putExtra(SitioPesca.propiedadPaginaWeb,sitio.getUrlPagina());
                    startActivity(intentPaginaPrincipal);
                }
                else
                {
                    Toast.makeText(MapaSitiosPesca.this,"El sitio solicitado no tiene una pagina definida", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
