package com.example.etsisi.fishpot.firebaseLogin;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.etsisi.fishpot.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ProfileInfoEntry extends AppCompatActivity
        implements      View.OnClickListener,
                        AdapterView.OnItemSelectedListener,
                        View.OnFocusChangeListener, View.OnTouchListener {

    private FirebaseAuth firebaseAuth;

    private TextView textViewUserEmail;
    private EditText editTextName;

    private Button buttonSaveInfo;
    private DatabaseReference databaseReference;

    private Spinner spProvincias;
    private Spinner spLocalidades;
    private boolean editado1erSpinner;

    private RadioGroup radioGroupSexo;

    private RadioButton radioButtonHombre;
    private RadioButton radioButtonMujer;

    private EditText editTextEdad;

    private UserInformation userInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_info_entry);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(this, Login.class));
        }

        FirebaseUser user = firebaseAuth.getCurrentUser();

        textViewUserEmail = (TextView) findViewById(R.id.textViewUserEmail);

        textViewUserEmail.setText(user.getEmail());

        userInfo = new UserInformation();


        editTextName = (EditText) findViewById(R.id.editTextName);
        buttonSaveInfo = (Button) findViewById(R.id.saveInformation);

        editado1erSpinner = false;
        spProvincias = (Spinner) findViewById(R.id.sp_provincia);
        spLocalidades = (Spinner) findViewById(R.id.sp_localidad);

        radioGroupSexo = (RadioGroup) findViewById(R.id.radioGroupSexo);
        editTextEdad = (EditText) findViewById(R.id.editTextEdad);

        radioButtonHombre = (RadioButton) findViewById(R.id.radioButtonHombre);
        radioButtonMujer = (RadioButton) findViewById(R.id.radioButtonMujer);

        loadSpinnerProvincias();

        // Gestionamos las pulsaciones sobre los distintos elementos.
        buttonSaveInfo.setOnClickListener(this);

        // Gestionamos cuando cambia el focus sobre los distintos elementos.
        editTextName.setOnFocusChangeListener(this);
        editTextEdad.setOnFocusChangeListener(this);

        // Gestionamos cuando se pulsa el boton siguiente del teclado
        editTextName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionID, KeyEvent event) {
                if (actionID == EditorInfo.IME_ACTION_NEXT) {
                    //do your stuff here...
                    hideKeyboard(view);
                    view.clearFocus();
                    spProvincias.requestFocus();
                    spProvincias.performClick();
                    return true;
                }
                return false;
            }
        });

        // Gestionamos cuando se pulsa el botón "done" del teclado numerico
        editTextEdad.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionID, KeyEvent event) {
                if (actionID == EditorInfo.IME_ACTION_DONE) {
                    //do your stuff here...
                    hideKeyboard(view);
                    view.clearFocus();
                    return true;
                }
                return false;
            }
        });

        radioGroupSexo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                editTextEdad.clearFocus();
                hideKeyboard(editTextEdad);
            }
        });
        spProvincias.setOnTouchListener(this);
        spLocalidades.setOnTouchListener(this);

    }

    private void loadSpinnerProvincias() {

        // Create an ArrayAdapter using the string array and a default spinner
        // layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.provincias, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        this.spProvincias.setAdapter(adapter);

        // This activity implements the AdapterView.OnItemSelectedListener
        this.spProvincias.setOnItemSelectedListener(this);
        this.spLocalidades.setOnItemSelectedListener(this);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos,
                               long id) {
        switch (parent.getId()) {
            case R.id.sp_provincia:
                editado1erSpinner = true;

                // Retrieves an array
                TypedArray arrayLocalidades = getResources().obtainTypedArray(
                        R.array.array_provincia_a_localidades);
                CharSequence[] localidades = arrayLocalidades.getTextArray(pos);
                arrayLocalidades.recycle();

                // Create an ArrayAdapter using the string array and a default
                // spinner layout
                ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(
                        this, android.R.layout.simple_spinner_item,
                        android.R.id.text1, localidades);

                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                // Apply the adapter to the spinner
                this.spLocalidades.setAdapter(adapter);

                if(!TextUtils.isEmpty(editTextName.getText().toString())) {
                    view.clearFocus();
                    spLocalidades.requestFocus();
                    spLocalidades.performClick();

                }

                break;

            case R.id.sp_localidad:
                    if(!editado1erSpinner && TextUtils.isEmpty(editTextEdad.getText().toString())){
                        view.clearFocus();
                        editTextEdad.requestFocus();
                    }else {
                        editado1erSpinner = false;
                    }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Callback method to be invoked when the selection disappears from this
        // view. The selection can disappear for instance when touch is
        // activated or when the adapter becomes empty.

    }

    @Override
    public void onClick(View v) {
        if(v == buttonSaveInfo){
            saveUserInformation();
            finish();
            startActivity(new Intent(this, ProfileInfoShow.class).putExtra("infoProfileObj", userInfo));
        }
    }

    private void saveUserInformation(){

        if(checkEntries() == false){
            // Si las entradas no son validas, acabamos la ejecución del proceso de guardado;
            return;
        }
        userInfo.name = editTextName.getText().toString().trim();
        userInfo.email = firebaseAuth.getCurrentUser().getEmail();
        userInfo.provincia = spProvincias.getSelectedItem().toString().trim();
        userInfo.localidad = spLocalidades.getSelectedItem().toString().trim();
        userInfo.edad = Integer.parseInt(editTextEdad.getText().toString().trim());
        int radioButtonID = radioGroupSexo.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) radioGroupSexo.findViewById(radioButtonID);
        userInfo.sexo = radioButton.getText().toString().trim();

        databaseReference = FirebaseDatabase.getInstance().getReference();

        FirebaseUser user = firebaseAuth.getCurrentUser();

        if(user == null) {
            Toast.makeText(this, "Error en el guardado... El usuario no esta logueado.", Toast.LENGTH_LONG).show();
            return;
        }

        databaseReference.child(user.getUid()).setValue(userInfo);
        Toast.makeText(this, "Información guardada correctamente", Toast.LENGTH_LONG).show();
    }

    private boolean checkEntries(){
        if(TextUtils.isEmpty(editTextName.getText().toString().trim())){
            Toast.makeText(this, "Introduzca un Nombre y Apellidos", Toast.LENGTH_LONG).show();
            return false;
        } else if(TextUtils.isEmpty(editTextEdad.getText().toString().trim())){
            Toast.makeText(this, "Introduzca su edad", Toast.LENGTH_LONG).show();
            return false;
        } else if(radioGroupSexo.getCheckedRadioButtonId() == -1){
            Toast.makeText(this, "Introduzca su género", Toast.LENGTH_LONG).show();
            return false;
        } else{
            return true;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus) {
            hideKeyboard(v);
        }
        if(v == editTextEdad && hasFocus){
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    // Cuando se toca el spinner, cerramos los posibles teclados
    // y limpiamos los focus de los editText que tengan teclado
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP && (v == spProvincias ||  v == spLocalidades) ) {
            hideKeyboard(v);
            editTextEdad.clearFocus();
            editTextName.clearFocus();
        }
        return false;
    }
}
