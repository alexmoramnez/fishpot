package com.example.etsisi.fishpot.firebaseLogin;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alejandro on 30/05/2017.
 */

public class UserInformation implements Parcelable{

    public String name;
    public String email;
    public String provincia;
    public String localidad;
    public int edad;
    public String sexo;

    public UserInformation() {
    }

    public UserInformation(Parcel in ) {
        readFromParcel( in );
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public UserInformation createFromParcel(Parcel in ) {
            return new UserInformation( in );
        }

        public UserInformation[] newArray(int size) {
            return new UserInformation[size];
        }
    };

    public UserInformation(String name,
                           String email,
                           String provincia,
                           String localidad,
                           int edad,
                           String sexo) {
        this.name = name;
        this.email = email;
        this.provincia = provincia;
        this.localidad = localidad;
        this.edad = edad;
        this.sexo = sexo;
    }

    @Override
    public int describeContents() {
        return 6;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(provincia);
        dest.writeString(localidad);
        dest.writeInt(edad);
        dest.writeString(sexo);

    }

    private void readFromParcel(Parcel in ) {

        name        = in .readString();
        email       = in .readString();
        provincia   = in .readString();
        localidad   = in .readString();
        edad        = in .readInt();
        sexo        = in .readString();
    }
}
