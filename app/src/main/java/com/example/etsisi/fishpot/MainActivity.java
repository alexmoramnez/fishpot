package com.example.etsisi.fishpot;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.etsisi.fishpot.modelos.SitioPesca;
import com.example.etsisi.fishpot.servicios.ServicioPosicionGps;
import com.example.etsisi.fishpot.servicios.ServicioPosicionGpsAntiguo;

public class MainActivity extends AppCompatActivity {

    private TextView txtEmbalse;
    private TextView txtPagina;
    private Button botonMapa;
    private WebView webView1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        txtEmbalse =(TextView) findViewById(R.id.txtEmbalse);
        botonMapa = (Button) findViewById(R.id.btnMapa);
        webView1 = (WebView) findViewById(R.id.webView1);

        botonMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent abrirMapa = new Intent(MainActivity.this, MapaSitiosPesca.class);
                startActivity(abrirMapa);
            }
        });
        if(intent != null)
        {
            String text = intent.getStringExtra(SitioPesca.propiedadNombre);
            if(text != null) {
                txtEmbalse.setText(text);
            }
            text = intent.getStringExtra(SitioPesca.propiedadPaginaWeb);
            if(text != null) {
                webView1.setWebViewClient(new WebViewClient());
                webView1.loadUrl(text);
            }
        }

    }
    @Override
    protected void onStop()
    {
        super.onStop();
        if(!this.estaServicioActivo(ServicioPosicionGps.class)) {
            startService(new Intent(this, ServicioPosicionGps.class));
            //Toast.makeText(this,"El servicio se ha iniciado",Toast.LENGTH_LONG).show();
        }

    }
@Override
    protected void onDestroy()
    {
        super.onDestroy();
        this.stopService(new Intent(this,ServicioPosicionGps.class));
    }
    private  boolean estaServicioActivo(Class<?> claseServicio)
    {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (claseServicio.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;

    }
}
