package com.example.etsisi.fishpot.modelos;

/**
 * Created by Riki Gomez on 20/04/2017.
 */

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.geometry.Point;
import com.google.maps.android.kml.KmlPoint;

/**
 * Clase que representa un sitio de pesca
 */
public class SitioPesca {

    public static final String propiedadNombre ="name";
    public static final  String propiedadDescripccion ="description";
    public static final String  propiedadPaginaWeb ="paginaWeb";
    private String nombre;
    private String urlPagina;
    private KmlPoint puntoMapa;
    private LatLng latLng;
    private String descripcion;
    public SitioPesca(String nombre, String urlPagina,KmlPoint puntoMapa,String descripcion)
    {
        this.nombre = nombre;
        this.urlPagina = urlPagina;
        this.puntoMapa = puntoMapa;
        latLng = puntoMapa.getGeometryObject();
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getUrlPagina()
    {
        return urlPagina;
    }

    public void setUrlPagina(String urlPagina)
    {
        this.urlPagina = urlPagina;
    }

    public KmlPoint getPuntoMapa()
    {
        return puntoMapa;
    }

    public void setPuntoMapa(String coordenadas)
    {
        this.puntoMapa = puntoMapa;
    }

    public LatLng getLatLng()
    {
        return latLng;
    }

    public void setLatLng(LatLng latLng)
    {
        this.latLng = latLng;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
