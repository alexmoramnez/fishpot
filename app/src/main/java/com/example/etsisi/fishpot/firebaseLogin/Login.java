package com.example.etsisi.fishpot.firebaseLogin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.etsisi.fishpot.MainActivity;
import com.example.etsisi.fishpot.MenuPrincipal;
import com.example.etsisi.fishpot.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private Button buttonSignIn;
    private EditText editTextMail;
    private EditText editTextPassword;
    private TextView textViewSignUp;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        // Si el usuario ya está logeado, mostramos profile activity.
        if(firebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), ProfileInfoEntry.class ));
        }

        buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
        editTextMail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById((R.id.editTextPassword));
        textViewSignUp = (TextView) findViewById(R.id.textViewSignUp);

        progressDialog = new ProgressDialog(this);

        buttonSignIn.setOnClickListener(this);
        textViewSignUp.setOnClickListener(this);

        editTextMail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                                  @Override
                                                  public void onFocusChange(View v, boolean hasFocus) {
                                                      if(!hasFocus) {
                                                          hideKeyboard(v);
                                                      }
                                                  }
                                              }
        );

        editTextPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                                      @Override
                                                      public void onFocusChange(View v, boolean hasFocus) {
                                                          if(!hasFocus) {
                                                              hideKeyboard(v);
                                                          }
                                                      }
                                                  }
        );

    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void userLogin() {
        String email = editTextMail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            ///empty e-mail
            Toast.makeText(this, "Please Enter e-mail", Toast.LENGTH_SHORT).show();
            //stop the execution
            return;
        }
        if(TextUtils.isEmpty(password)){
            //password is empty
            Toast.makeText(this, "Please Enter password", Toast.LENGTH_SHORT).show();
            //stop the execution
            return;
        }

        progressDialog.setMessage("Singing In... Please Wait");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();

                        if(task.isSuccessful()){
                            //start the profile activity
                            finish();
                            startActivity(new Intent(getApplicationContext(), MenuPrincipal.class ));
                        }
                    }
                });

    }

    @Override
    public void onClick(View v) {
        if(v == buttonSignIn){
            userLogin();
        }
        else if(v == textViewSignUp) {
            startActivity(new Intent(this, Register.class));
        }
    }
}