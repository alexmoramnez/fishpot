package com.example.etsisi.fishpot;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.example.etsisi.fishpot.MainActivity;
import com.example.etsisi.fishpot.R;
import com.example.etsisi.fishpot.firebaseLogin.Login;
import com.example.etsisi.fishpot.firebaseLogin.ProfileInfoShow;
import com.example.etsisi.fishpot.firebaseLogin.Register;
import com.google.firebase.auth.FirebaseAuth;


public class PantallaInicio extends AppCompatActivity implements View.OnClickListener {


    private Button buttonRegister;
    private Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(FirebaseAuth.getInstance().getCurrentUser() != null){
            finish();
            startActivity(new Intent(this, MenuPrincipal.class));
        }

        setContentView(R.layout.activity_pantalla_inicio);

        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        buttonRegister.setOnClickListener(this);
        buttonLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == buttonRegister){
            startActivity(new Intent(this, Register.class));
        } else if (v == buttonLogin){
            startActivity(new Intent(this, Login.class));
        }
    }
}
