package com.example.etsisi.fishpot.servicios;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.example.etsisi.fishpot.MainActivity;
import com.example.etsisi.fishpot.R;
import com.example.etsisi.fishpot.modelos.SitioPesca;
import com.example.etsisi.fishpot.util.GoogleMapsUtil;
import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class ServicioPosicionGps extends Service {

    /**
     * Radio de busqueda
     */
    /**
     * Radio de busqueda KM
     */
    public static int RADIO_BUSQUEDA = 2;

    /**
     * Numero de km  a comprobar si hay nuevos sitios de pesca
     */
    private static final double METROSCOMPROBAR = 10;
    /**
     * Parametro con el valor del tiempo de espera que se le pasa en el intent
     */
    public static String parametroTiempoEspera = "tiempoEspera";


    /**
     * Equivalencia en segundos de un minuto
     */
    private static final int MINUTOS = 60;

    /**
     * Equivalencia de milisegundos de un segundo
     */
    private static final int MILISEGUNDOS = 1000;
    /**
     * Tiempo de espera hasta que se vuelve otra vez a comprobar la posición, en minutos
     */
    private long tiempoEspera;
    private int veces;
    private List<SitioPesca> listaSitiosPesca;


    public ServicioPosicionGps() {
        super();
        tiempoEspera = 20;
    }

    private class LocalizadorSitiosPesca implements LocationListener {


        private List<SitioPesca> listaSitioPescas;
        /**
         * Mapa ordenado con los sitios de pesca por cercania al dipositivo
         */
        private TreeMap<Double, SitioPesca> mapaOrdenadoSitiosPesca;

        public LocalizadorSitiosPesca(List<SitioPesca> listaSitiosPesca) {
            this.listaSitioPescas = listaSitiosPesca;
            mapaOrdenadoSitiosPesca = new TreeMap<Double, SitioPesca>();

        }

        /**
         * Called when the location has changed.
         * <p>
         * <p> There are no restrictions on the use of the supplied Location object.
         *
         * @param location The new location, as a Location object.
         */
        @Override
        public void onLocationChanged(Location location) {

            //Obtenemos la posicion actual
            LatLng posicionACtual = new LatLng(location.getLatitude(), location.getLongitude());
            //Recorremos la lista de sitios  calculando la distancia a los sitios que estan dentro del radio de busqueda
            for (SitioPesca sitio : listaSitioPescas) {
                double distancia = this.calcularDistanciaPuntos(posicionACtual, sitio.getLatLng());
                if (distancia <= RADIO_BUSQUEDA ) {//el sitio esta dentro del radio de busqueda
                    mapaOrdenadoSitiosPesca.put(distancia, sitio);
                }
            }

            //Lanzamos una notificacion al movil diciendo si quiere iniciar la aplicación con el primer  sitio del mapa(el sitio mas proximo)
            if (!mapaOrdenadoSitiosPesca.isEmpty()) {
                this.lanzarAplicacion(mapaOrdenadoSitiosPesca.firstEntry().getKey(), mapaOrdenadoSitiosPesca.firstEntry().getValue());
            }


        }

        /**
         * Se lanza la aplicación con el sitio mas próximo
         * @param distancia distancia del usuario
         * @param sitio
         */
        private void lanzarAplicacion(Double distancia, SitioPesca sitio) {

            //creamos la notificación
            //creamos la accion
            Intent intentLanzaApp = new Intent(ServicioPosicionGps.this.getApplicationContext(), MainActivity.class);
            intentLanzaApp.putExtra(SitioPesca.propiedadNombre, sitio.getNombre());
            intentLanzaApp.putExtra(SitioPesca.propiedadPaginaWeb, sitio.getUrlPagina());

            PendingIntent resultadoIntent = PendingIntent.getActivity(ServicioPosicionGps.this.getApplicationContext(), 0, intentLanzaApp, PendingIntent.FLAG_UPDATE_CURRENT);
            //creamos la notificacion
            String distanciaF = String.format("%.3f",Math.round(distancia*1000.0)/1000.0);
            String mensajeNotificacion = "El sitio " + sitio.getNombre() +  " esta a " + distanciaF +". Click para abrir la aplicacion";
            Notification notificacion = new NotificationCompat.Builder(ServicioPosicionGps.this.getApplicationContext()).setContentTitle(ServicioPosicionGps.this.getResources().getString(R.string.app_name)).setContentIntent(resultadoIntent).setSmallIcon(R.mipmap.ic_launcher).setStyle(new NotificationCompat.BigTextStyle().bigText(mensajeNotificacion)).build();
            //Lanzamos la notificion
            NotificationManager notificationManager = (NotificationManager) ServicioPosicionGps.this.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(001, notificacion);


        }

        private double calcularDistanciaPuntos(LatLng puntoInicio, LatLng puntoFinal) {
            int Radius = 6371;// radius of earth in Km
            double lat1 = puntoInicio.latitude;
            double lat2 = puntoFinal.latitude;
            double lon1 = puntoInicio.longitude;
            double lon2 = puntoFinal.longitude;
            double dLat = Math.toRadians(lat2 - lat1);
            double dLon = Math.toRadians(lon2 - lon1);
            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                    + Math.cos(Math.toRadians(lat1))
                    * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                    * Math.sin(dLon / 2);
            double c = 2 * Math.asin(Math.sqrt(a));
            double valueResult = Radius * c;
            double km = valueResult / 1;
            DecimalFormat newFormat = new DecimalFormat("####");
            int kmInDec = Integer.valueOf(newFormat.format(km));
            double meter = valueResult % 1000;
            int meterInDec = Integer.valueOf(newFormat.format(meter));
            System.out.println("Radius Value:" + valueResult + "   KM  " + kmInDec
                    + " Meter   " + meterInDec);

            return Radius * c;

        }

        /**
         * Called when the provider status changes. This method is called when
         * a provider is unable to fetch a location or if the provider has recently
         * become available after a period of unavailability.
         *
         * @param provider the name of the location provider associated with this
         *                 update.
         * @param status   {@link LocationProvider#OUT_OF_SERVICE} if the
         *                 provider is out of service, and this is not expected to change in the
         *                 near future; {@link LocationProvider#TEMPORARILY_UNAVAILABLE} if
         *                 the provider is temporarily unavailable but is expected to be available
         *                 shortly; and {@link LocationProvider#AVAILABLE} if the
         *                 provider is currently available.
         * @param extras   an optional Bundle which will contain provider specific
         *                 status variables.
         *                 <p>
         *                 <p> A number of common key/value pairs for the extras Bundle are listed
         *                 below. Providers that use any of the keys on this list must
         *                 provide the corresponding value as described below.
         *                 <p>
         *                 <ul>
         *                 <li> satellites - the number of satellites used to derive the fix
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        /**
         * Called when the provider is enabled by the user.
         *
         * @param provider the name of the location provider associated with this
         *                 update.
         */
        @Override
        public void onProviderEnabled(String provider) {

        }

        /**
         * Called when the provider is disabled by the user. If requestLocationUpdates
         * is called on an already disabled provider, this method is called
         * immediately.
         *
         * @param provider the name of the location provider associated with this
         *                 update.
         */
        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    public void onCreate() {
        veces = 0;
        //TODO: Obtener la lista de sitios de pesca dado el archivo kmlfile. Usar para ello el parser
        // https://developer.android.com/training/basics/network-ops/xml.html
        this.obtenerListaSitiosPesca();
    }

    private void obtenerListaSitiosPesca() {
        this.listaSitiosPesca = new ArrayList<SitioPesca>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            InputStream kmlFile = this.getResources().openRawResource(R.raw.kmlfile);
            Document documento = dBuilder.parse(kmlFile);
            NodeList listaSitios = documento.getElementsByTagName("Placemark");
            for (int i = 0; i < listaSitios.getLength(); i++) {
                Node sitio = listaSitios.item(i);
                if (sitio.getNodeType() == Node.ELEMENT_NODE) {
                    Element elementoHijo = (Element) sitio;
                    //Obtenemos los atributos
                    String nombre = getValue("name", elementoHijo);
                    String descripcion = getValue("description", elementoHijo);
                    String coordenadas = getValue("coordinates", elementoHijo).replaceAll("\n", "").replaceAll(" ", "");
                    listaSitiosPesca.add(GoogleMapsUtil.crearSitioPesca(nombre, descripcion, coordenadas));
                }
            }

        } catch (ParserConfigurationException e) {
            Log.d(ServicioPosicionGps.class.getName(), "No se ha podido obtener el DOM Parser" + e.getMessage());
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private String getValue(String tag, Element elemento) {
        NodeList nodeList = elemento.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        Toast.makeText(ServicioPosicionGps.this, "El servicio esta iniciado: " + veces, Toast.LENGTH_LONG).show();
        LocationListener localizadorSitiosPesca = new ServicioPosicionGps.LocalizadorSitiosPesca(this.listaSitiosPesca);
        //Lanzamos el listener para detectar sitios de pesca
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return START_STICKY;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, this.tiempoEspera * MINUTOS * MILISEGUNDOS,(float) METROSCOMPROBAR, localizadorSitiosPesca);
        veces++;
        return START_STICKY;
    }


    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * {@link IBinder} is usually for a complex interface
     * that has been <a href="{@docRoot}guide/components/aidl.html">described using
     * aidl</a>.
     * <p>
     * <p><em>Note that unlike other application components, calls on to the
     * IBinder interface returned here may not happen on the main thread
     * of the process</em>.  More information about the main thread can be found in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html">Processes and
     * Threads</a>.</p>
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
